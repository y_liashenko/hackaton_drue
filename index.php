<?php 
error_reporting(0);
error_reporting(E_ERROR | E_PARSE);

session_start();

if(isset($_GET['logout'])){//TODO: csrf-token
    session_destroy();
    header("Location: ./index.php");
    die();
}

if (isset($_SESSION['id'])){
    header("Location: ./queue.php");
    die();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Control Panel Login</title>
    <link rel="stylesheet" href="Semantic-UI/semantic.min.css">
    <script src="js/jquery-2.2.0.min.js"></script>
    <script src="Semantic-UI/semantic.min.js"></script>
    <script src="js/js.js"></script>
</head>
<body>

    <div class="ui container">
        <h2 class="header">Dr's Log In</h2>
<?php 
if(isset($_POST['login']) AND isset($_POST['pass'])){
    require_once('sql.php');
    
    $error = false;
    $error_descr = 'Failed login with login/pass pair';
    $login = $_POST['login'];
    $pass = $_POST['pass'];
    
    if($arr = dr_auth($login, $pass)){
        $name = $arr['name'];
        $id = $arr['id'];
        $_SESSION['name'] = $name;
        $_SESSION['id'] = $id;
        header("Location: ./queue.php");
        die();
    }else{
        $error = true;
    }

}

?>
        <div class="ui divider"></div>
    <form class="ui form segment" method="post" action="./index.php">
        <div class="field">
            <label>Login</label>
            <input type="text" name="login" placeholder="Login">
        </div>
        <div class="field">
            <label>Password</label>
            <input type="text" name="pass" placeholder="Password">
        </div>
        <button class="ui button" type="submit">Submit</button>
        <?php if($error): ?>
            <span style="color:red; font-size: 16px;"><?php echo $error_descr; ?></span>
        <?php endif; ?>
    </form>
</div>

</body>
</html>
