<?php 
error_reporting(0);
error_reporting(E_ERROR | E_PARSE);

session_start();

if(!isset($_SESSION['id']))
    die('access forbidden');

$doc_id = $_SESSION['id'];

require_once('./sql.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Pending queue</title>
    <link rel="stylesheet" href="Semantic-UI/semantic.min.css">
    <link rel="stylesheet" href="Semantic-UI/components/modal.min.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="js/jquery-2.2.0.min.js"></script>
    <script src="Semantic-UI/semantic.min.js"></script>
    <script src="Semantic-UI/components/colorize.min.js"></script>
    <script src="js/js.js"></script>
</head>
<body style="padding-top:20px;">

<div class="ui container">

    <h2 class="header">Manage</h2>
    <div class="ui divider"></div>
    <div class="segment" style="overflow: hidden">
        <button href="./index.php?logout=1" class="ui right floated button red"><a style='color:white;' href="./index.php?logout=1&csrf=must_be_secure">Log out</a></button>
    </div>

    <div class="ui divider"></div>

<?php 
    
        update_last_activity($doc_id);
        $requests = requests($doc_id);
        if($requests !== FALSE):
?>
    <table id="q_table" class="ui selectable celled table">
        <thead>
        <tr>
            <th>#</th>
            <th>First/Last Name</th>
            <th>Medical card</th>
            <th>Checked in</th>
        </tr></thead>
        <tbody>
        <?php 
        $count = 1;
        ?>
        <?php foreach($requests as $k => $val): ?>
        <?php if($val['telegram_uid']): ?>
        <tr class="tableRow" data-userId="<?php echo $val['telegram_uid']; ?>">
            <td>
                <div class="ui ribbon label"><?php echo $count; ?></div>
            </td>
            <td><?php echo patient_name($val['telegram_uid']); ?>
                <button id="buttonCancel-<?php echo $val['telegram_uid']; ?>" data-html="<div class='ui form form-size'><div class='field'><label>Summarize result</label><textarea class='textCancel' rows='6'></textarea></div><button class='buttonReasonRefusal ui button' type='submit'>Submit</button></div>" class="cancel ui button red right floated" style="display: none">Process
                    <div id="clockdiv-<?php echo $val['telegram_uid'];?>">
                        <span class="seconds"></span>
                    </div>
                </button>
                <button class="accept ui button green right floated">Accept</button>
                <!--<div class="clear-both"></div>-->

            </td>
            <td class="followPpc">
                <!--<button class="ui button">Follow</button>-->

                <a href="#" data-userId="<?php echo $val['telegram_uid']; ?>" class="med_card">Open</a>
            </td>
            <td><?php echo get_date_diff((int)$val['created_at'], time());?> ago</td>
        </tr>
        <?php $count++; ?>
        <?php endif;?>
        <?php endforeach; ?>
        </tbody>
        <tfoot>
        </tfoot>
    </table>
<?php else: ?>
    <?php echo '<h3>Queue empty</h3>' ?>
<?php endif; ?>
<div class="ui modal">
  <i class="close icon"></i>
  <div class="header">
    Medical card
  </div>
    <div id="modal-description" style="padding:20px;">
        
    </div>
  </div>
</div>

</div>


<?php 
function get_date_diff( $time1, $time2, $precision = 2 ) {
    // If not numeric then convert timestamps
    if( !is_int( $time1 ) ) {
        $time1 = strtotime( $time1 );
    }
    if( !is_int( $time2 ) ) {
        $time2 = strtotime( $time2 );
    }
    // If time1 > time2 then swap the 2 values
    if( $time1 > $time2 ) {
        list( $time1, $time2 ) = array( $time2, $time1 );
    }
    // Set up intervals and diffs arrays
    $intervals = array( 'year', 'month', 'day', 'hour', 'minute', 'second' );
    $diffs = array();
    foreach( $intervals as $interval ) {
        // Create temp time from time1 and interval
        $ttime = strtotime( '+1 ' . $interval, $time1 );
        // Set initial values
        $add = 1;
        $looped = 0;
        // Loop until temp time is smaller than time2
        while ( $time2 >= $ttime ) {
            // Create new temp time from time1 and interval
            $add++;
            $ttime = strtotime( "+" . $add . " " . $interval, $time1 );
            $looped++;
        }
        $time1 = strtotime( "+" . $looped . " " . $interval, $time1 );
        $diffs[ $interval ] = $looped;
    }
    $count = 0;
    $times = array();
    foreach( $diffs as $interval => $value ) {
        // Break if we have needed precission
        if( $count >= $precision ) {
            break;
        }
        // Add value and interval if value is bigger than 0
        if( $value > 0 ) {
            if( $value != 1 ){
                $interval .= "s";
            }
            // Add value and interval to times array
            $times[] = $value . " " . $interval;
            $count++;
        }
    }
    // Return string with times
    return implode( ", ", $times );
}

?>
<script>
    $(document).ready(function() {
        $('.cancel').popup({on: 'click'});
    });
</script>

</body>
</html>
