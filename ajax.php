<?php 

error_reporting(0);
error_reporting(E_ERROR | E_PARSE);

define('BOT_TOKEN', '145933078:AAG_UwiR5yzBhNPHfSa9kqlniy8eojt-yz8');
define('API_URL', 'https://api.telegram.org/bot'.BOT_TOKEN.'/');


session_start();

header('Content-Type: application/json');

require_once("./sql.php");

if (isset($_SESSION['id'])){
    $doc_id = $_SESSION['id'];
    $doc_name = $_SESSION['name'];
    update_last_activity($doc_id);

    if(isset($_POST['action']) AND $_POST['action'] == 'accept' AND isset($_POST["user_id"])){
        $chat_id = get_chat_id($_POST["user_id"]);
         
      apiRequestJson("sendMessage", 
        array(
        'chat_id' => $chat_id, 
        "text" => '*Dr '.$doc_name.' waiting for you, be in time, please.*',
        'parse_mode' => 'Markdown',
        'reply_markup' => 
            array(
            'hide_keyboard' => true)));
               // 'keyboard' => array(array('Ok, i am going')),
               // 'one_time_keyboard' => true,
               // 'resize_keyboard' => true)));
     die();   
    }

    //such a nice code
    if(isset($_POST['action']) AND $_POST['action'] == 'processed' AND isset($_POST["user_id"])){
        $process_res = (isset($_POST['processed_result']) AND !empty($_POST['processed_result'])) ? $_POST['processed_result'] : 'Without description';
        $process_res_DB = "<b>".date("Y:m:d").", ".$doc_name.":</b><br>".$process_res."<br><br>";
        $chat_id = get_chat_id($_POST["user_id"]);
        
        free_state2($_POST['user_id']);
        remove_patient_from_queue($_POST['user_id'], 'doesn\'t metter');
    
        $med_card = med_card($_POST['user_id']);
        update_med_card($med_card . $process_res_DB, $_POST['user_id']);

       apiRequestJson("sendMessage", 
        array(
        'chat_id' => $chat_id, 
        "text" => '*'.date('Y:m:d').', '.$doc_name.':*'.chr(10).$process_res,
        'parse_mode' => 'Markdown',
        'reply_markup' => 
            array(
                'keyboard' => array(array('Ok(Check out), bye!')),
                'one_time_keyboard' => true,
                'resize_keyboard' => true)));
        
        die();   
    }
    if(isset($_GET['action']) AND $_GET['action'] == 'med_card' AND isset($_GET['user_id'])){
        $med_card = med_card($_GET['user_id']);
        
        echo json_encode(array('med_card' => $med_card)); 
    }
    if(isset($_POST['action']) AND $_POST['action'] == 'queue_update'){
        $requests = requests($doc_id);
    $html = '';
    $count = 1;
    foreach($requests as $k => $val){
        if($val['telegram_uid']){
            $html = $html . '<tr class="tableRow" data-userId="'.$val['telegram_uid'].'"><td><div class="ui ribbon label">'.$count.'</div></td><td>'.patient_name($val['telegram_uid']).'<button id="buttonCancel-'.$val['telegram_uid'].'" data-html="<div class=\'ui form form-size\'><div class=\'field\'><label>Processed result</label><textarea class=\'textCancel\' rows=\'6\'></textarea></div><button class=\'buttonReasonRefusal ui button\' type=\'submit\'>Submit</button></div>" class="cancel ui button red right floated" style="display: none">Process<div id="clockdiv-'.$val['telegram_uid'].'"><span class="seconds"></span></div></button><button class="accept ui button green right floated">Accept</button></td><td class="followPpc"><a href="#" class="not-active">Follow</a></td><td>'.get_date_diff((int)$val['created_at'], time()).' ago</td></tr>';
            $count++;
        }
    }
        echo json_encode(array('html' => $html));
        die();
        
    }
}else{
    echo json_encode(array("sucess" => false, "error_descr" => "Login error"));    
}


function get_date_diff( $time1, $time2, $precision = 2 ) {
    // If not numeric then convert timestamps
    if( !is_int( $time1 ) ) {
        $time1 = strtotime( $time1 );
    }
    if( !is_int( $time2 ) ) {
        $time2 = strtotime( $time2 );
    }
    // If time1 > time2 then swap the 2 values
    if( $time1 > $time2 ) {
        list( $time1, $time2 ) = array( $time2, $time1 );
    }
    // Set up intervals and diffs arrays
    $intervals = array( 'year', 'month', 'day', 'hour', 'minute', 'second' );
    $diffs = array();
    foreach( $intervals as $interval ) {
        // Create temp time from time1 and interval
        $ttime = strtotime( '+1 ' . $interval, $time1 );
        // Set initial values
        $add = 1;
        $looped = 0;
        // Loop until temp time is smaller than time2
        while ( $time2 >= $ttime ) {
            // Create new temp time from time1 and interval
            $add++;
            $ttime = strtotime( "+" . $add . " " . $interval, $time1 );
            $looped++;
        }
        $time1 = strtotime( "+" . $looped . " " . $interval, $time1 );
        $diffs[ $interval ] = $looped;
    }
    $count = 0;
    $times = array();
    foreach( $diffs as $interval => $value ) {
        // Break if we have needed precission
        if( $count >= $precision ) {
            break;
        }
        // Add value and interval if value is bigger than 0
        if( $value > 0 ) {
            if( $value != 1 ){
                $interval .= "s";
            }
            // Add value and interval to times array
            $times[] = $value . " " . $interval;
            $count++;
        }
    }
    // Return string with times
    return implode( ", ", $times );
}


//------Bot api
function apiRequestWebhook($method, $parameters) {
  if (!is_string($method)) {
    error_log("Method name must be a string\n");
    return false;
  }

  if (!$parameters) {
    $parameters = array();
  } else if (!is_array($parameters)) {
    error_log("Parameters must be an array\n");
    return false;
  }

  $parameters["method"] = $method;

  header("Content-Type: application/json");
  echo json_encode($parameters);
  return true;
}

function exec_curl_request($handle) {
  $response = curl_exec($handle);

  if ($response === false) {
    $errno = curl_errno($handle);
    $error = curl_error($handle);
    error_log("Curl returned error $errno: $error\n");
    curl_close($handle);
    return false;
  }

  $http_code = intval(curl_getinfo($handle, CURLINFO_HTTP_CODE));
  curl_close($handle);

  if ($http_code >= 500) {
    // do not wat to DDOS server if something goes wrong
    sleep(10);
    return false;
  } else if ($http_code != 200) {
    $response = json_decode($response, true);
    error_log("Request has failed with error {$response['error_code']}: {$response['description']}\n");
    if ($http_code == 401) {
      throw new Exception('Invalid access token provided');
    }
    return false;
  } else {
    $response = json_decode($response, true);
    if (isset($response['description'])) {
      error_log("Request was successfull: {$response['description']}\n");
    }
    $response = $response['result'];
  }

  return $response;
}

function apiRequest($method, $parameters) {
  if (!is_string($method)) {
    error_log("Method name must be a string\n");
    return false;
  }

  if (!$parameters) {
    $parameters = array();
  } else if (!is_array($parameters)) {
    error_log("Parameters must be an array\n");
    return false;
  }

  foreach ($parameters as $key => &$val) {
    // encoding to JSON array parameters, for example reply_markup
    if (!is_numeric($val) && !is_string($val)) {
      $val = json_encode($val);
    }
  }
  $url = API_URL.$method.'?'.http_build_query($parameters);

  $handle = curl_init($url);
  curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
  curl_setopt($handle, CURLOPT_TIMEOUT, 60);

  return exec_curl_request($handle);
}

function apiRequestJson($method, $parameters) {
  if (!is_string($method)) {
    error_log("Method name must be a string\n");
    return false;
  }

  if (!$parameters) {
    $parameters = array();
  } else if (!is_array($parameters)) {
    error_log("Parameters must be an array\n");
    return false;
  }

  $parameters["method"] = $method;

  $handle = curl_init(API_URL);
  curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
  curl_setopt($handle, CURLOPT_TIMEOUT, 60);
  curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($parameters));
  curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));

  return exec_curl_request($handle);
}

