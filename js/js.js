$(document).ready(function() {
    var acceptClick = $('.accept'),
        //cancelClick = $('.cancel'),
        tableRow = $('.tableRow'),
        followPpc = $('.followPpc'),
        notActive = $('.not-active'),
        userId,
        flag2 = true,
        flag = false;
        
    setTimeout(executeQuery, 5000);

    function executeQuery() {
      // $.post("./ajax.php",
      //  {
      //      action: "queue_update"
      //  },
      //  function(data){
            if(flag == false){
                location.reload();//
//                $('#q_table .tableRow').remove();
//                $('#q_table tr:last').after(data.html);
//                console.log(data.html);
            }
       // });
      setTimeout(executeQuery, 5000); // you could choose not to continue on failure...
    }

    $('body').on('click', '.med_card', function(){
        var curr_id = $(this).closest("tr", tableRow).data('userid');
        $.get( "./ajax.php", {user_id: curr_id , action: "med_card"} )
          .done(function( data ) {
              if(data.med_card == ''){
                $('#modal-description').html("<p><b>Empty medical card</b></p>");
               }
               else{
                $('#modal-description').html("<p>"+data.med_card+"</p>");
              }
              console.log(data);
        });
    $('.ui.modal').modal({
        onHide: function(){
                flag = false;
            
        },
        onShow: function(){
            console.log("before show: " + flag);
            flag = true;
            console.log("after show" + flag);
        }    
    }).modal('show');
                    
    });

    $('body').on('click', '.accept', function() {
        userId = $(this).closest("tr", tableRow).data('userid');
        if(flag == false) {
            //$(acceptClick).addClass('loading disabled');
            $(this).hide();
            $(this).closest("tr", tableRow).addClass('warning');
            $('#buttonCancel-'+userId).show();
            $(followPpc).find(notActive).removeClass('not-active');

            //var userId = $(this).closest("tr", tableRow).data('userid');
            console.log(userId);

            flag = true;

            setTimeout(function() { //cancelClick.addClass('disabled');
                $('#buttonCancel-'+userId).addClass('disabled');
                initializeClock('clockdiv-'+userId, deadline);
            }, 0);
            setTimeout(function() { $('#clockdiv-'+ userId).hide();
                $('#buttonCancel-'+userId).removeClass('disabled');}, 7000);
                clearTimeout();

            // countdown
            function getTimeRemaining(endtime) {
                var t = Date.parse(endtime) - Date.parse(new Date());
                var seconds = Math.floor((t / 1000) % 10);
                return {
                    'total': t,
                    'seconds': seconds
                };
            }
            function initializeClock(id, endtime) {
                var clock = document.getElementById(id);
                var secondsSpan = clock.querySelector('.seconds');

                function updateClock() {
                    var t = getTimeRemaining(endtime);
                    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
                    if (t.total <= 0) {
                        clearInterval(timeinterval);
                    }
                }
                updateClock();
                var timeinterval = setInterval(updateClock, 1000);
                hide;
            }
            var deadline = new Date(Date.parse(new Date()) + 7 * 1000);
            var hide = $('#buttonCancel-'+userId).removeClass('disabled');
            initializeClock('clockdiv-' + userId, deadline);

            $.post("./ajax.php",
                {
                    user_id: userId,
                    action: "accept"
                },
                function(user_id, action){
                    console.log("user_id: " + userId + "action: " + action);
                });
        }
    });

    $('body').on('click', '.buttonReasonRefusal', function() {
        flag = false;
        var textMessage = $(this).closest('div').find('.textCancel').val();
        $('.popup').modal('hide');
        $('.warning').hide();
        $.post("./ajax.php",
            {
                user_id: userId,
                action: "processed",
                processed_result: textMessage
                
            },
            function(user_id, action){
                console.log("user_id: " + userId + "action: " + action);
            });
    });

 $('.pause').on('click', function() {
        if(flag == false) {
            return false;
        } else  {
            $('.accept').removeClass('disabled');
            $(this).text('Pause').removeClass('unpause');
        }
        if(flag == true) {
            return false;
        } else  {
            $('.accept').addClass('disabled');
            $(this).text('Unpause').addClass('unpause');
        }

    });

});
